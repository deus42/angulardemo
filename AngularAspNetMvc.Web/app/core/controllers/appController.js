var app;
(function (app) {
    (function (core) {
        (function (controllers) {
            var CoreController = (function () {
                function CoreController() {
                }
                return CoreController;
            })();
            controllers.CoreController = CoreController;
        })(core.controllers || (core.controllers = {}));
        var controllers = core.controllers;
    })(app.core || (app.core = {}));
    var core = app.core;
})(app || (app = {}));
//# sourceMappingURL=appController.js.map
