module app.directives {

    export class BaseDirective implements ng.IDirective {

        public priority: number;
        public template: string;
        public templateUrl: string;
        public replace: boolean;
        public transclude: any;
        public restrict: string;
        public scope: any;
        public link: Function;
        public compile: Function;
        public controller: Function;
        public isEmpty: (value: any) => boolean;
        public require: string;
        constructor() {
            this.isEmpty = function (value) {
                return angular.isUndefined(value) || value === '' || value === null || value !== value;
            };
        }
    }

    export class NoClickDirective extends app.directives.BaseDirective {

        constructor() {
            super();
            this.restrict = 'A';
            this.link = function (scope: ng.IScope, element: ng.IAugmentedJQuery) {
                element.click(function (eventObject: JQueryEventObject) {
                    eventObject.preventDefault();
                });
            };
        }
    }

    angular.module('app.directives.Common', [])
        .directive('noClick', [function () {
            return new app.directives.NoClickDirective();
        }]);
}