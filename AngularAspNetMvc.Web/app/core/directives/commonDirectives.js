var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var app;
(function (app) {
    (function (directives) {
        var BaseDirective = (function () {
            function BaseDirective() {
                this.isEmpty = function (value) {
                    return angular.isUndefined(value) || value === '' || value === null || value !== value;
                };
            }
            return BaseDirective;
        })();
        directives.BaseDirective = BaseDirective;

        var NoClickDirective = (function (_super) {
            __extends(NoClickDirective, _super);
            function NoClickDirective() {
                _super.call(this);
                this.restrict = 'A';
                this.link = function (scope, element) {
                    element.click(function (eventObject) {
                        eventObject.preventDefault();
                    });
                };
            }
            return NoClickDirective;
        })(app.directives.BaseDirective);
        directives.NoClickDirective = NoClickDirective;

        angular.module('app.directives.Common', []).directive('noClick', [
            function () {
                return new app.directives.NoClickDirective();
            }
        ]);
    })(app.directives || (app.directives = {}));
    var directives = app.directives;
})(app || (app = {}));
//# sourceMappingURL=commonDirectives.js.map
