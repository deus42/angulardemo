/// <reference path="home/services/menuService.ts" />
/// <reference path="home/interfaces.ts" />
/// <reference path="../Scripts/typings/angularjs/angular.d.ts" />
var app;
(function (app) {
    var appUtils = (function () {
        function appUtils() {
        }
        appUtils.createViewUrl = function (fragmnt, globals) {
            return globals.baseUrl + fragmnt + '?v=' + globals.version;
        };
        return appUtils;
    })();
    angular.module('app', [
        'ngRoute',
        'app.globalsModule',
        'app.home.controllers',
        'app.contacts.contactsController',
        'app.contacts.contactsTypesController',
        'app.directives.Common',
        'app.home.services'
    ]).config([
        '$routeProvider',
        '$windowProvider',
        'globalsServiceProvider',
        'app.home.menuServiceProvider',
        function ($routeProvider, $window, globalsServiceProvider, menuServiceProvider) {
            var globals = globalsServiceProvider.$get();
            var injector = angular.injector(['app.home.services']);
            var menuService = injector.get('app.home.menuService');
            menuService.getMenu().success(function (data) {
                angular.forEach(data, function (menu) {
                    $routeProvider.when(menu.Path, {
                        controller: menu.Controller,
                        templateUrl: appUtils.createViewUrl(menu.TemplateUrl, globals)
                    });
                });
                $routeProvider.otherwise({
                    redirectTo: '/home'
                });
            }).error(function (error) {
                $window.alert('Could not get the menu');
            });
        }
    ]);
})(app || (app = {}));
//# sourceMappingURL=app.js.map
