/// <reference path="home/services/menuService.ts" />
/// <reference path="home/interfaces.ts" />
/// <reference path="../Scripts/typings/angularjs/angular.d.ts" />
module app {

    class appUtils {
        static createViewUrl(fragmnt: string, globals: interfaces.IGLobals) {
            return globals.baseUrl + fragmnt + '?v=' + globals.version;
        }
    }

    angular.module('app',
        [
            'app.globalsModule',
            'app.home.controllers',
            'app.contacts.contactsController',
            'app.contacts.contactsTypesController',
            'app.directives.Common',
            'app.home.services',
			'ngRoute'
        ])
        .config(['$routeProvider', '$windowProvider', 'globalsServiceProvider', 'app.home.menuServiceProvider',
            function ($routeProvider: ng.IRouteProvider, $window: ng.IWindowService, globalsServiceProvider: interfaces.IGlobalsProvider, menuServiceProvider: app.home.services.IMenuServiceProvider) {
                var globals: interfaces.IGLobals = globalsServiceProvider.$get();
                var menuService: app.home.services.IMenuService = menuServiceProvider.$get();
                menuService.getMenu()
                    .success(function (data) {
                        angular.forEach(data, function (menu: interfaces.IMenuItem) {
                            $routeProvider.when(menu.Path, {
                                controller: menu.Controller,
                                templateUrl: appUtils.createViewUrl(menu.TemplateUrl, globals)
                            });

                        });
                        $routeProvider.otherwise({
                            redirectTo: '/home'
                        });
                    })
                    .error(function (error) {
                        $window.alert('Could not get the menu');
                    });
            }]);
}