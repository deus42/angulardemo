var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var app;
(function (app) {
    /// <reference path="../../core/services/utilities.ts" />
    /// <reference path="../../home/interfaces.ts" />
    /// <reference path="../../core/controllers/appController.ts" />
    /// <reference path="../../../Scripts/typings/angularjs/angular.d.ts" />
    (function (contacts) {
        

        var ContactsController = (function (_super) {
            __extends(ContactsController, _super);
            function ContactsController($scope, utilities) {
                _super.call(this);
                this.$scope = $scope;
                this.utilities = utilities;
                $scope.message = function () {
                    utilities.showMessage('Hello');
                };
            }
            return ContactsController;
        })(app.core.controllers.CoreController);

        angular.module('app.contacts.contactsController', ['app.core.services.utilities']).controller('app.contacts.contactsController', [
            '$scope',
            'utilities',
            function ($scope, utilities) {
                return new ContactsController($scope, utilities);
            }
        ]);
    })(app.contacts || (app.contacts = {}));
    var contacts = app.contacts;
})(app || (app = {}));
//# sourceMappingURL=contactsController.js.map
