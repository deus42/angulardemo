/// <reference path="../../core/services/utilities.ts" />
/// <reference path="../../home/interfaces.ts" />
/// <reference path="../../core/controllers/appController.ts" />
/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts" />
module app.contacts {

    import IUtilities = app.core.services.IUtilities;

    interface IContactScope extends ng.IScope {
        message: () => void;
    }

    class ContactsController extends app.core.controllers.CoreController {

        constructor(private $scope: IContactScope, private utilities: IUtilities) {
            super();
            $scope.message = function () {
                utilities.showMessage('Hello');
            };
        }
    }

    angular.module('app.contacts.contactsController', ['app.core.services.utilities'])
        .controller('app.contacts.contactsController', ['$scope', 'utilities', function ($scope: IContactScope, utilities: IUtilities) {
            return new ContactsController($scope, utilities);
        }]);
}