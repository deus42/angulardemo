/// <reference path="../../home/interfaces.ts" />
/// <reference path="../../core/controllers/appController.ts" />
/// <reference path="../../../Scripts/typings/angularjs/angular.d.ts" />
module app.contacts {

    class contactsTypesController extends app.core.controllers.CoreController {
    }

    angular.module('app.contacts.contactsTypesController', [])
        .controller('app.contacts.contactsTypesController', function () {
            return new contactsTypesController();
        });
}