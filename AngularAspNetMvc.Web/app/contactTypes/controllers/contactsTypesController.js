var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var app;
(function (app) {
    /// <reference path="../../home/interfaces.ts" />
    /// <reference path="../../core/controllers/appController.ts" />
    /// <reference path="../../../Scripts/typings/angularjs/angular.d.ts" />
    (function (contacts) {
        var contactsTypesController = (function (_super) {
            __extends(contactsTypesController, _super);
            function contactsTypesController() {
                _super.apply(this, arguments);
            }
            return contactsTypesController;
        })(app.core.controllers.CoreController);

        angular.module('app.contacts.contactsTypesController', []).controller('app.contacts.contactsTypesController', function () {
            return new contactsTypesController();
        });
    })(app.contacts || (app.contacts = {}));
    var contacts = app.contacts;
})(app || (app = {}));
//# sourceMappingURL=contactsTypesController.js.map
