var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var app;
(function (app) {
    (function (home) {
        /// <reference path="../services/menuService.ts" />
        /// <reference path="../../../Scripts/typings/angularjs/angular.d.ts" />
        /// <reference path="../../core/controllers/appController.ts" />
        (function (controllers) {
            var homeController = (function (_super) {
                __extends(homeController, _super);
                function homeController($location, $scope, $route, $timeout, menuService) {
                    _super.call(this);
                    this.$location = $location;
                    this.$scope = $scope;
                    this.menuService = menuService;
                    $scope.navigate = function (path) {
                        $location.path(path);
                    };

                    menuService.getMenu().success(function (data) {
                        $scope.menus = new Array();
                        angular.forEach(data, function (menu) {
                            $scope.menus.push(menu);
                        });
                        $scope.$apply();
                        var stop = $timeout(function () {
                            if ($route.routes['/home']) {
                                $route.reload();
                                $timeout.cancel(stop);
                            }
                        }, 50);
                    }).error(function (result, status, headers, config) {
                        if (status != 0) {
                            alert('error');
                        }
                    });
                }
                return homeController;
            })(app.core.controllers.CoreController);

            angular.module('app.home.controllers', ['app.home.services']).controller('homeController', [
                '$location',
                '$scope',
                '$route',
                '$timeout',
                'app.home.menuService',
                function ($location, $scope, $route, $timeout, menuService) {
                    return new homeController($location, $scope, $route, $timeout, menuService);
                }
            ]);
        })(home.controllers || (home.controllers = {}));
        var controllers = home.controllers;
    })(app.home || (app.home = {}));
    var home = app.home;
})(app || (app = {}));
//# sourceMappingURL=homeController.js.map
