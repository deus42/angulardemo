var app;
(function (app) {
    (function (home) {
        /// <reference path="../interfaces.ts" />
        /// <reference path="../../../Scripts/typings/angularjs/angular.d.ts" />
        (function (services) {
            var MenuService = (function () {
                function MenuService($http, globals) {
                    this.$http = $http;
                    this.globals = globals;
                    this.getMenu = function () {
                        return $http.get(globals.webApiBaseUrl + '/home/menu');
                    };
                }
                return MenuService;
            })();

            angular.module('app.home.services', []).factory('app.home.menuService', function () {
                var injector = angular.injector(['ng', 'app.globalsModule']);
                var $http = injector.get('$http');
                var globals = injector.get('globalsService');
                return new MenuService($http, globals);
            });
        })(home.services || (home.services = {}));
        var services = home.services;
    })(app.home || (app.home = {}));
    var home = app.home;
})(app || (app = {}));
//# sourceMappingURL=menuService.js.map
