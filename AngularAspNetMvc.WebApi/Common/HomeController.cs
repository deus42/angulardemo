﻿using System.Web.Http;
using AngularAspNetMvc.Models.Common;
using AngularAspNetMvc.WebApi.Core;
using System.Collections.Generic;

namespace AngularAspNetMvc.WebApi.Common
{
    public class HomeController : RelayController
    {
        [HttpGet]
        public List<MenuItem> Menu()
        {
            return new List<MenuItem>
            {
                new MenuItem
                {
                    Path = "/home",
                    Controller = "homeController",
                    TemplateUrl = "home/home",
                    Title = "Home"
                },
                new MenuItem
                {
                    Path = "/contacttypes",
                    Controller = "app.contacts.contactsTypesController",
                    TemplateUrl = "contacttypes/index",
                    Title = "Contact Types"
                },
                new MenuItem
                {
                    Path = "/contacts",
                    Controller = "app.contacts.contactsController",
                    TemplateUrl = "contacts/index",
                    Title = "Contacts"
                }
            };
        }

      
    }
}
