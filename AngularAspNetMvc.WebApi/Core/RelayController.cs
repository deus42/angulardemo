﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace AngularAspNetMvc.WebApi.Core
{
    public class RelayController : ApiController
    {
        public override Task<HttpResponseMessage> ExecuteAsync(
            HttpControllerContext controllerContext,
            CancellationToken cancellationToken)
        {
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["Redirect"] ?? "false"))
            {
                var url = controllerContext.Request.RequestUri;
                url = new Uri(url.AbsoluteUri.Replace(
                  ConfigurationManager.AppSettings["OriginalUriFragment"],
                  ConfigurationManager.AppSettings["ReplacemenUriFragment"]));

                var client = new HttpClient();
                client.DefaultRequestHeaders.Clear();
                foreach (var httpRequestHeader in controllerContext.Request.Headers)
                {
                    client.DefaultRequestHeaders.Add(httpRequestHeader.Key, httpRequestHeader.Value);
                }
                if (controllerContext.Request.Method == HttpMethod.Get)
                {
                    return client.GetAsync(url, cancellationToken);
                }
                if (controllerContext.Request.Method == HttpMethod.Post)
                {
                    return client.PostAsync(url, controllerContext.Request.Content, cancellationToken);
                }
                if (controllerContext.Request.Method == HttpMethod.Delete)
                {
                    return client.DeleteAsync(url, cancellationToken);
                }
                if (controllerContext.Request.Method == HttpMethod.Put)
                {
                    return client.PutAsync(url, controllerContext.Request.Content, cancellationToken);
                }
                throw new NotSupportedException("Unknown method passed to Relay Controller");
            }

            return base.ExecuteAsync(controllerContext, cancellationToken);
        }
    }
}
