﻿using AngularAspNetMvc.Models.Core;
namespace AngularAspNetMvc.Models.Contacts
{
    public class Contact : EditableModel
    {
        public int ContactId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool IsActive { get; set; }

        public int ContactTypeId { get; set; }

    }
}
