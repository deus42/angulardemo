﻿using AngularAspNetMvc.Models.Core;
namespace AngularAspNetMvc.Models.Contacts
{
    public class ContactType : EditableModel
    {
        public int ContactTypeId { get; set; }

        public string Name { get; set; }

    }
}
