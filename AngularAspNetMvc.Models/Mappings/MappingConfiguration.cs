﻿using System.Linq;
using AngularAspNetMvc.Data.Models;
using AutoMapper;

namespace AngularAspNetMvc.Models.Mappings
{
    public static class MappingConfiguration
    {
        public static void CreateMaps()
        {
            CreateContactMaps();

#if DEBUG
            Mapper.AssertConfigurationIsValid();
#endif
        }

        private static void CreateContactMaps()
        {
            CreateTwoWayMapping<ContactType, Contacts.ContactType>();
            CreateTwoWayMapping<Contact, Contacts.Contact>();
            Mapper.CreateMap<ContactInfo, Contacts.ContactInfo>();
        }

        private static void CreateTwoWayMapping<TSource, TDestination>()
        {
            Mapper.CreateMap<TSource, TDestination>()
                .IgnoreAllNonExisting();

            Mapper.CreateMap<TDestination, TSource>()
                .IgnoreAllNonExisting();
        }

        private static void IgnoreAllNonExisting<TSource, TDestination>(
            this IMappingExpression<TSource, TDestination> expression)
        {
            var sourceType = typeof(TSource);
            var destinationType = typeof(TDestination);
            var existingMaps = Mapper.GetAllTypeMaps().First(
                x => x.SourceType == sourceType && x.DestinationType == destinationType);
            foreach (var property in existingMaps.GetUnmappedPropertyNames())
            {
                expression.ForMember(property, opt => opt.Ignore());
            }
        }
    }
}
