﻿using System.Collections.Generic;
using AngularAspNetMvc.DataAccess.Core.Repository;
using AngularAspNetMvc.Models.Contacts;

namespace AngularAspNetMvc.DataAccess.Repository
{
    public interface IContactTypeRepository : IWriteRepository
    {
        IEnumerable<ContactType> GetAllContactTypes();

        ContactType GetContactType(int contactTypeId);
    }
}