﻿using System.Collections.Generic;
using System.Linq;
using AngularAspNetMvc.Models.Contacts;
using AutoMapper;


namespace AngularAspNetMvc.DataAccess.Repository
{
    public class ContactRepository : Core.Repository, IContactRepository
    {
        public IEnumerable<ContactInfo> GetContacts(int pageNumber, int rowsPerPage, string name)
        {
            return Context.Contacts
                .OrderBy(one => one.LastName)
                .ThenBy(one => one.FirstName)
                .Select(one => new Data.Models.ContactInfo
                {
                    ContactId = one.ContactId,
                    FirstName = one.FirstName,
                    LastName = one.LastName
                })
                .ToList()
                .Select(Mapper.Map<Data.Models.ContactInfo, ContactInfo>);
        }

        public Contact GetContact(int contactId)
        {
            return Mapper.Map<Data.Models.Contact, Contact>(GetByPrimaryKey<Data.Models.Contact>(contactId));
        }
    }
}
