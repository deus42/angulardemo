﻿using System.Collections.Generic;
using System.Linq;
using AngularAspNetMvc.Models.Contacts;
using AutoMapper;


namespace AngularAspNetMvc.DataAccess.Repository
{
    public class ContactTypeRepository : Core.Repository, IContactTypeRepository
    {
        public IEnumerable<ContactType> GetAllContactTypes()
        {
            return Context.ContactTypes.OrderBy(one => one.Name)
                .Select(Mapper.Map<Data.Models.ContactType, ContactType>);
        }

        public ContactType GetContactType(int contactTypeId)
        {
            return Mapper.Map<Data.Models.ContactType, ContactType>(
                GetByPrimaryKey<Data.Models.ContactType>(contactTypeId));
        }
    }
}
