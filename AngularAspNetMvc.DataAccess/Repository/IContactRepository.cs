using System.Collections.Generic;
using AngularAspNetMvc.DataAccess.Core.Repository;
using AngularAspNetMvc.Models.Contacts;

namespace AngularAspNetMvc.DataAccess.Repository
{
    public interface IContactRepository : IWriteRepository
    {
        IEnumerable<ContactInfo> GetContacts(int pageNumber, int rowsPerPage, string name);
        Contact GetContact(int contactId);
    }
}