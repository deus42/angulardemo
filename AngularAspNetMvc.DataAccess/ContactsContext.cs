﻿using System.Data.Entity;
using AngularAspNetMvc.Data.Models;
using AngularAspNetMvc.DataAccess.Config;

namespace AngularAspNetMvc.DataAccess
{
    public class ContactsContext : DbContext
    {
        public ContactsContext()
            :base("Name=ContactsDb")
        {
            
        }

        public DbSet<Contact> Contacts { get; set; }

        public DbSet<ContactType> ContactTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ContactConfig());
            modelBuilder.Configurations.Add(new ContactTypeConfig());
        }
    }
}
