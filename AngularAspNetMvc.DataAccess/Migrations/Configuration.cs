using AngularAspNetMvc.Data.Models;

namespace AngularAspNetMvc.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public sealed class Configuration : DbMigrationsConfiguration<ContactsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ContactsContext context)
        {
            context.ContactTypes.AddOrUpdate(
                p => p.Name,
                new[]
                {
                    new ContactType{Name = "Friend"},
                    new ContactType{Name = "Coworker"},
                    new ContactType{Name = "Family"}
                }
            );

        }
    }
}
